# Algorytmy i Struktury Danych
Repozytorium z materiałami


# Praca domowa (opcjonalna) pomiędzy zajęciami:

## Dokończenie z zajęć:  
* Sortowanie przez scalanie  
* Sortowanie szybkie 
## Lista:  
* 	https://www.codewars.com/kata/53dbd5315a3c69eed20002dd   
* 	https://www.codewars.com/kata/56f3a1e899b386da78000732 
## Słownik:  
* 	https://www.codewars.com/kata/5a21e090f28b824def00013c   
## Rekurencja (z zapamiętywaniem):  
* 	Tutaj jednak bez zadanka, gdyż jakkolwiek bym nie szukał to zdecydowana większość przypadków, gdzie jest to wykorzystywane jest albo ciągiem fibonacciego, albo czystą sztuką akademicką :). PS. Jak jednak, ktoś nie będzie tego chciał odpuścić niech napisze prywatną wiadomość.  
* 	https://www.codewars.com/kata/sum-of-a-sequence/python  (tutaj polecam realizację zadania w formie rekurencji a nie iteracji ^^)  
## Algorytmy:  
* 	https://www.yworks.com/downloads#yEd (jest wersja zarówno windows jak i Mac) lub wersja online https://www.yworks.com/yed-live/
* 	A następnie rozrysować 1-2 algorytmy (z tych, które były na zajęciach lub z pracki domowej) jako schemat blokowy (polecam wysłać mi schemat, bez mówienia jaki problem rozwiązuje dany schemat, a ja wtedy zweryfikuję czy jest on 'samotłumaczący się' :D
## Zadanie dodatkowe (jakby ktoś już zrealizował wszystko):  
* Porównanie prędkości działania algorytmów sortujących:  
-  dla wszystkich 4 algorytmów na wejściu podać kopię tej samej nieposortowanej listy ( najlepiej wygenerować dość dużą korzystając z biblioteki random) (do pomiaru czasu można wykorzystać bibliotekę time, a przykład użycia w komentarzu 2giej prezentacji, a dokładniej w filmie o wadach)  
-  porównać wyniki z dostarczaną funkcją w pythonie np. sorted()
