
# Syntax Errors

def some_function()
    msg = "hello, world!"
    print(msg)
     return msg

def some_function():
	msg = "hello, world!"
	print(msg)
        return msg


# Variable Name Errors
print(a)

print(hello)

for number in range(10):
    count = count + number
print("The count is:", count)

Count = 0
for number in range(10):
    count = count + number
print("The count is:", count)

# Index Errors
letters = ['a', 'b', 'c']
print("Letter #1 is", letters[0])
print("Letter #2 is", letters[1])
print("Letter #3 is", letters[2])
print("Letter #4 is", letters[3])

# File Errors
file_handle = open('myfile.txt', 'r')
file_handle = open('myfile.txt', 'w')
file_handle.read()

#debugger
# Here is the source code of the Python Programto print odd numbers within a given range.
#(4, 9) (4,8)
lower=int(input("Enter the lower limit for the range:"))
upper=int(input("Enter the upper limit for the range:"))
for i in range(lower,upper-1):
    if(i%2!=0):
        print(i)

# The program takes a number n and prints an identity matrix of the desired size.
# http://www.coolmath.com/sites/cmat/files/images/05-matrices-16.gif
n=int(input("Enter a number: "))
for i in range(0,n):
    for j in range(0,n):
        if(i!=j):
            print("1",sep=" ",end=" ")
        else:
            print("0",sep=" ",end=" ")
    print()


### Test 1
# 1. How many levels does the traceback have?
# 2. What is the function name where the error occurred?
# 3. On which line number in this function did the error occur?
# 4. What is the type of error?
# 5. What is the error message?
# This code has an intentional error. Do not type it directly;
# use it for reference to understand the error message below.
def print_message(day):
    messages = {
        "monday": "Hello, world!",
        "tuesday": "Today is Tuesday!",
        "wednesday": "It is the middle of the week.",
        "thursday": "Today is Donnerstag in German!",
        "friday": "Last day of the week!",
        "saturday": "Hooray for the weekend!",
        "sunday": "Aw, the weekend is almost over."
    }
    print(messages[day])

def print_friday_message():
    print_message("Friday")

print_friday_message()


### Test 2
# Read the code below, and (without running it) try to identify what the errors are.
# Run the code, and read the error message. Is it a SyntaxError or an IndentationError?
# Fix the error.
# Repeat steps 2 and 3, until you have fixed all the errors.
def another_function
  print("Syntax errors are annoying.")
   print("But at least Python tells us about them!")
  print("So they are usually not too hard to fix.")

### Test 3
# Read the code below, and (without running it) try to identify what the errors are.
# Run the code, and read the error message. What type of NameError do you think this is? In other words, is it a string with no quotes, a misspelled variable, or a variable that should have been defined but was not?
# Fix the error.
# Repeat steps 2 and 3, until you have fixed all the errors.
for number in range(10):
    # use a if the number is a multiple of 3, otherwise use b
    if (Number % 3) == 0:
        message = message + a
    else:
        message = message + "b"
print(message)


### Test 4
# Read the code below, and (without running it) try to identify what the errors are.
# Run the code, and read the error message. What type of error is it?
# Fix the error.
seasons = ['Spring', 'Summer', 'Fall', 'Winter']
print('My favorite season is ', seasons[4])

### Test 5
# Upside down (7)
n=int(input("Enter number of rows: "))
for i in range (n, 0,-1):
    print((n-i) * ' ' + 2 * i * '*')


### Test 6
# Compare 2 values
def which_is_bigger(val_1, val_2):
    if val_1 > val_2:
        print("Val_1 is higher!")
    else:
        print("Val_2 is higher!")

num1 = input('Enter first number: ')
num2 = input('Enter second number: ')
which_is_bigger(num1, num2)