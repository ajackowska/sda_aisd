import pandas as pd
# import xlrd as xlrdd
# pandas i xlrd musza zostać doinstalowane do korzystania z tej opcji

data = pd.read_excel (r'sda_example.xlsx') #(use "r" before the path string to address special character, such as '\'). Don't forget to put the file name at the end of the path + '.xlsx'
data_frame = pd.DataFrame(data, columns= ['samochod', 'marka', 'kolor'])
auta = [data_frame.columns.values.tolist()] + data_frame.values.tolist()

f = '{:<10}|{:<10}|{:<10}' # format
for i in auta:
    print(f.format(*i))

for i in auta:
    if i[0] == 'Sluzbowy':
        print("Mam sluzbowe auto na wakacje jade za darmo!")