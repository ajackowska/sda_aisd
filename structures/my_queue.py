class MyQueue(object):
    def __init__(self, maximum):
        self.maximum = maximum
        self.items = []

    def isEmpty(self):
        return None

    def enqueue(self, item):
        return None

    def dequeue(self):
        return None

    def size(self):
        return None

    def peek(self):
        return None

    def print(self):
        print(self.items)

if __name__ == "__main__":
    que = MyQueue(5)
    que.print()
    que.enqueue(3)
    que.enqueue(4)
    que.enqueue(9)
    que.enqueue(7)
    que.enqueue(2)
    que.enqueue(0)

    que.print()
    print("After taking from queue")
    que.dequeue()
    que.dequeue()
    que.print()
    que.dequeue()
    que.dequeue()
    que.dequeue()
    que.dequeue()
    que.print()