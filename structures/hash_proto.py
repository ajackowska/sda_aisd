
class hashClass:
    def __init__(self):
        # TODO 2 shouldn't I think about list of list?
        self.hash_table = [None] * 10

    def hashing_func(self, key):
        # TODO 1 Since Python is so awesome, aren't here any building hash function?
        return key % len(self.hash_table)

    def insert(self, key, value):
        hash_key = self.hashing_func(key)
        # TODO 2 Wait what if we have collision?
        self.hash_table[hash_key] = value

    def print(self):
        print(self.hash_table)
        print()

    def search(self, key):
        # TODO 3 Please implement :)
        return None

    def delete(self, key):
        # TODO 4 Please implement :)
        return None

if __name__ == "__main__":
    sda_hash = hashClass()
    sda_hash.print()

    sda_hash.insert(10, 'Nepal')
    sda_hash.print()

    sda_hash.insert(25, 'USA')
    sda_hash.print()

    # TODO 1 uncomment to see if problem 1 solved
    # sda_hash.insert('40', 'Canada')
    # sda_hash.print()

    # TODO 2 uncomment to see if problem 2 solved
    # sda_hash.insert(20, 'Brazil')
    # sda_hash.print()

    # TODO 3 Validate problem 3
    print(sda_hash.search(10))
    print(sda_hash.search(20))
    print(sda_hash.search(30))
    print()

    # TODO 4 Validate problem 4
    sda_hash.delete(10)
    sda_hash.print()
    sda_hash.delete(10)
    sda_hash.print()
